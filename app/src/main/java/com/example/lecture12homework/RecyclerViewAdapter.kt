package com.example.lecture12homework

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recycler_view_layout.view.*

class RecyclerViewAdapter(private val items: MutableList<ItemModel>) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recycler_view_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount() = items.size

    private lateinit var model: ItemModel

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            model = items[adapterPosition]
            itemView.imageView.setImageResource(model.image)
            itemView.nameTextView.text = model.name
            itemView.descriptionTextView.text = model.description
        }
    }
}

