package com.example.lecture12homework

class ItemModel(
    val image: Int,
    val name: String,
    val description: String
)