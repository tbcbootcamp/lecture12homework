package com.example.lecture12homework

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val items = mutableListOf<ItemModel>()
    private lateinit var adapter: RecyclerViewAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        adapter = RecyclerViewAdapter(items)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        setData()
    }

    private fun setData(){
        items.add(ItemModel(R.mipmap.queen, "Queen", "Since 1970"))
        items.add(ItemModel(R.mipmap.rammstein, "Rammstein", "Since 1994"))
        items.add(ItemModel(R.mipmap.slipknot, "Slipknot", "Since 1995"))
        items.add(ItemModel(R.mipmap.a7x, "Avenged Sevenfold", "Since 1999"))
        items.add(ItemModel(R.mipmap.acdc, "AC DC", "Since 1973"))
        items.add(ItemModel(R.mipmap.queen, "Queen", "Since 1970"))
        items.add(ItemModel(R.mipmap.rammstein, "Rammstein", "Since 1994"))
        items.add(ItemModel(R.mipmap.slipknot, "Slipknot", "Since 1995"))
        items.add(ItemModel(R.mipmap.a7x, "Avenged Sevenfold", "Since 1999"))
        items.add(ItemModel(R.mipmap.acdc, "AC DC", "Since 1973"))

    }

}
